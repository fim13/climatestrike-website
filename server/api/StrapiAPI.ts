import {map} from "lodash";

import {IStrapiConfig} from "./@def";
import {IPageModelStrategy, IPage} from "../../interfaces/model";
import {HttpService} from "../../core/HttpService";

export class StrapiAPI implements IPageModelStrategy {
    private $http = new HttpService();

    constructor(private config: IStrapiConfig) {}

    public async getPages() {
        const baseUrl = this.config.url;
        const rawPages = await this.$http.getd<IPage[]>(baseUrl + "/pages");
        const pages = map(rawPages, page => ({id: page.id, identifier: page.identifier, slug: page.slug, sortId: page.sortId, config: page.config}));
        return Promise.resolve(pages);
    }

    public async getContent(identifier: string, language: string) {
        const baseUrl = this.config.url;
        const pages = await this.$http.getd(baseUrl + `/pages?identifier=${identifier}&_limit=1`);
        const page = pages[0];
        const rawContent = page["content_" + language];
        const content = map(rawContent, (raw) => {
            const config = {...raw};
            delete config.id;
            delete config.__component;
            return {
                id: raw.id,
                identifier: raw.identifier,
                sortId: raw.sortId,
                type: (raw.__component as string).split(".").slice(1).join("."),
                config: config
            };
        });
        return Promise.resolve(content);
    }
}