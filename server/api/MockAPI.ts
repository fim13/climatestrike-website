export const a = null;
// import {TObjectId} from "../../interfaces/core";
// import {IPageModelStrategy, IPage, IContentItem, ELinkLocation} from "../../interfaces/model";
// import {IHomeItemProps, IImgProps, IEnumerationItemProps, IRichTextProps, IIframeProps, ITileItemProps, ILinkSectionProps, IEventsProps, IMapProps} from "../../components/@def";
// import {FacebookAPI} from "./FacebookAPI";
// import {sortBy} from "lodash";
// import {mapData, contactData} from "./MapData";


// export class MockAPI implements IPageModelStrategy {
//     public async getPages() {
//         return new Promise<IPage[]>(
//             (resolve) => {
//                 resolve([
//                     {id: "home", slug: "", identifier: "home", sortId: 0, config: {linkLocations: [ELinkLocation.header], rowModel: [1, 2, 2]}},
//                     {id: "facts", slug: "facts", identifier: "facts", sortId: 1, config: {linkLocations: [ELinkLocation.header]}},
//                     {id: "movement", slug: "movement", identifier: "movement", sortId: 2, config: {linkLocations: [ELinkLocation.header]}},
//                     {id: "contact", slug: "contact", identifier: "contact", sortId: 3, config: {linkLocations: [ELinkLocation.footer]}},
//                     {id: "media", slug: "media", identifier: "media", sortId: 4, config: {linkLocations: [ELinkLocation.footer]}},
//                     {id: "join", slug: "join", identifier: "join", sortId: 5, config: {linkLocations: [ELinkLocation.header, ELinkLocation.footer]}},
//                     {id: "regio", slug: "regio", identifier: "regio", sortId: 6, config: {linkLocations: [ELinkLocation.footer]}},
//                     {id: "events", slug: "events", identifier: "events", sortId: 7, config: {linkLocations: [ELinkLocation.header, ELinkLocation.footer]}},
//                     {id: "news", slug: "news", identifier: "news", sortId: 8, config: {linkLocations: [ELinkLocation.header]}},
//                     {id: "donate", slug: "donate", identifier: "donate", sortId: 9, config: {linkLocations: [ELinkLocation.header, ELinkLocation.footer], rowModel: [2]}},
//                     {id: "about", slug: "about", identifier: "about", sortId: 10, config: {linkLocations: [ELinkLocation.footnote]}},
//                     {id: "privacy", slug: "privacy", identifier: "privacy", sortId: 11, config: {linkLocations: [ELinkLocation.footnote]}}
//                 ]);
//             });
//     }

//     public async getContent(identifier: string, languageId: TObjectId) {
//         if (languageId === "de") {
//             if (identifier === "home") {
//                 return new Promise<IContentItem[]>((resolve) => {
//                     const nextStrike: IContentItem<IHomeItemProps> = {
//                         identifier: "next-strike",
//                         sortId: 1,
//                         type: "home-item",
//                         config: {
//                             title: "Nächster Streik!",
//                             subtitle: "Freitag, 29. November",
//                             image: {
//                                 identifier: "next-strike-img",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                                 position: "background"
//                             },
//                             link: {identifier: "next-strike-link", href: "/events", type: "more"}
//                         }
//                     };
//                     const news: IContentItem<IHomeItemProps> = {
//                         identifier: "news",
//                         sortId: 2,
//                         type: "home-item",
//                         config: {
//                             title: "News",
//                             description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
//                             image: {
//                                 identifier: "news-img",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                                 position: "background",
//                             },
//                             link: {identifier: "news-link", href: "/news", type: "more"}
//                         }
//                     };
//                     const why: IContentItem<IHomeItemProps> = {
//                         identifier: "why",
//                         sortId: 3,
//                         type: "home-item",
//                         config: {
//                             title: "Warum wir streiken",
//                             description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
//                             image: {
//                                 identifier: "why-img",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                                 position: "background",
//                             },
//                             link: {identifier: "why-link", href: "/movement", type: "more"}
//                         }
//                     };
//                     const facts: IContentItem<IHomeItemProps> = {
//                         identifier: "facts",
//                         sortId: 4,
//                         type: "home-item",
//                         config: {
//                             title: "Fakten",
//                             description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
//                             image: {
//                                 identifier: "facts-img",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                                 position: "background",
//                             },
//                             link: {identifier: "facts-link", href: "/facts", type: "more"}
//                         }
//                     };
//                     const join: IContentItem<IHomeItemProps> = {
//                         identifier: "join",
//                         sortId: 5,
//                         type: "home-item",
//                         config: {
//                             title: "Mitmachen",
//                             description: "Wir sind vor allem junge Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten um den Treibhauseffekt und seine Folgen seit Jahrzehnten bekannt sind.",
//                             image: {
//                                 identifier: "join-img",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                                 position: "background",
//                             },
//                             link: {identifier: "join-link", href: "/join", type: "more"}
//                         }
//                     };
//                     resolve([
//                         nextStrike,
//                         news,
//                         why,
//                         facts,
//                         join,
//                         nextStrike
//                     ]);
//                 });
//             }
//             else if (identifier === "facts") {
//                 return new Promise<IContentItem[]>((resolve) => {
//                     const nextStrike: IContentItem<IImgProps> = {
//                         identifier: "fact-1",
//                         sortId: 1,
//                         type: "img",
//                         config: {
//                             image: {
//                                 identifier: "fact-1-img",
//                                 src: "/static/images/de_facts-1.svg",
//                                 color: "white",
//                                 position: "background"
//                             },
//                             link: {identifier: "fact-1-link", href: "/events", type: "more"}
//                         }
//                     };
//                     resolve([
//                         nextStrike
//                     ]);
//                 });
//             }
//             else if (identifier === "join") {
//                 return new Promise<IContentItem[]>((resolve) => {
//                     const thanks: IContentItem<ITileItemProps> = {
//                         identifier: "thanks",
//                         sortId: 1,
//                         type: "tile-item",
//                         config: {
//                             title: "Danke",
//                             subtitle: "DASS DU Dich für das Klima einsetzt!",
//                             description: "Auch schon kleine Handlungen können Grosses bewirken!",
//                             icon: {
//                                 identifier: "heart",
//                                 position: "left",
//                                 src: "/static/images/heart.svg"
//                             },
//                             image: {
//                                 identifier: "join-0-img",
//                                 position: "alternating",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                             }
//                         }
//                     };
//                     const talk: IContentItem<IEnumerationItemProps> = {
//                         identifier: "talk",
//                         sortId: 2,
//                         type: "enumeration-item",
//                         config: {
//                             title: "Rede mit Leuten in deinem Umfeld über die Klimakrise",
//                             description: "Die Klimakrise geht jeden von uns etwas an. Umso mehr Leute darüber Bescheid wissen, umso grösser ist die Wahrscheinlichkeit dass wir es schaffen sie zu überwinden.",
//                             image: {
//                                 identifier: "join-1-img",
//                                 position: "alternating",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                             },
//                             link: {identifier: "join-1-link", href: "/facts", type: "facts"}
//                         }
//                     };
//                     const join: IContentItem<IEnumerationItemProps> = {
//                         identifier: "join",
//                         type: "enumeration-item",
//                         sortId: 3,
//                         config: {
//                             title: "Sei mit dabei",
//                             description: "Komm an die Demos und Streiks! Alle sind willkommen, egal ob jung oder alt! Am besten nimmst du gleich deine ganze Familie und alle deine Freund*innen mit.",
//                             image: {
//                                 identifier: "join-2-img",
//                                 position: "alternating",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                             },
//                             link: {identifier: "join-2-link", href: "/events", type: "events"}
//                         }
//                     };
//                     const network: IContentItem<IEnumerationItemProps> = {
//                         identifier: "network",
//                         type: "enumeration-item",
//                         sortId: 3,
//                         config: {
//                             title: "Vernetze dich mit anderen Menschen aus deiner Region",
//                             description: "Trete einem lokalen Klimastreik-Chat bei. Erfahre immer alles aus erster Hand und vernetze dich mit Menschen in deiner Nähe. Das Klima braucht dich!",
//                             image: {
//                                 identifier: "join-3-img",
//                                 position: "alternating",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                             },
//                             link: {identifier: "join-3-link", href: "/regio", type: "join chat"}
//                         }
//                     };
//                     const organize: IContentItem<IEnumerationItemProps> = {
//                         identifier: "organize",
//                         type: "enumeration-item",
//                         sortId: 4,
//                         config: {
//                             title: "Hilf bei der Organisation",
//                             description: "Um uns bei der Organisation zu unterstützen, trittst du am besten deiner Regionalgruppe bei. Dort wirst du weitere Informationen erhalten, wie du mithelfen kannst",
//                             image: {
//                                 identifier: "join-4-img",
//                                 position: "alternating",
//                                 src: "/static/images/49151584486_c18a65b564_c.jpg",
//                             },
//                             link: {identifier: "join-4-link", href: "/regio", type: "join chat"}
//                         }
//                     };
//                     const wiki: IContentItem<ILinkSectionProps> = {
//                         identifier: "wiki",
//                         type: "link-section",
//                         sortId: 5,
//                         config: {
//                             title: "wiki",
//                             description: "HIER FINDEST DU ALLE INFOS ÜBER DIE STRUKTUR UNSERER BEWEGUNG",
//                             link: {identifier: "join-5-link", href: "https://de.climatestrike.ch", type: "Wiki"}
//                         }
//                     };
//                     const download: IContentItem<ILinkSectionProps> = {
//                         identifier: "download",
//                         type: "link-section",
//                         sortId: 6,
//                         config: {
//                             title: "download",
//                             description: "Flyer, Logos & co.",
//                             link: {identifier: "join-5-link", href: "/downloads", type: "Downloads"}
//                         }
//                     };
//                     resolve([
//                         thanks,
//                         talk,
//                         join,
//                         network,
//                         organize,
//                         wiki,
//                         download
//                     ]);
//                 });
//             }
//             else if (identifier === "donate") {
//                 return new Promise<IContentItem[]>((resolve) => {
//                     const description: IContentItem<IRichTextProps> = {
//                         identifier: "donation-message",
//                         sortId: 1,
//                         type: "rich-text",
//                         config: {
//                             html: "<p>Im Moment sind wir auf Spenden angewiesen, um kleinere Dinge, wie beispielsweise Flyer, Plakate oder diese Website zu finanzieren.Viele von uns sind Schüler*innen oder Student*innen, deshalb ist unser finanzieller Spielraum beschränkt. Wir freuen uns über jede Spende!</p> <p>IBAN: CH28 0839 0036 0277 1000 2 <br> BIC: ABSOCH22XXX</p> <p>Climatestrike Switzerland<br>Hinterdorfstrasse 12<br>8635 Dürnten</p>"
//                         }
//                     };
//                     const item: IContentItem<IIframeProps> = {
//                         identifier: "donorbox",
//                         sortId: 2,
//                         type: "iframe",
//                         config: {
//                             src: "https://donorbox.org/embed/climatestrike-ch"
//                         }
//                     };
//                     resolve([
//                         description,
//                         item
//                     ]);
//                 });
//             }
//             else if (identifier === "events") {
//                 const fbApi = new FacebookAPI();
//                 const upcoming = await fbApi.getUpcomingEvents();
//                 const past = await fbApi.getPastEvents();
//                 return new Promise<IContentItem[]>((resolve) => {
//                     const events: IContentItem<IEventsProps> = {
//                         identifier: "event-1",
//                         sortId: 1,
//                         type: "events",
//                         config: {
//                             items: sortBy(past.concat(upcoming), event => event.startTime)
//                         }
//                     };
//                     resolve([events]);
//                 });
//             }
//         }
//         // Language independent
//         if (identifier === "regio") {
//             return new Promise<IContentItem[]>((resolve) => {
//                 const map: IContentItem<IMapProps> = {
//                     identifier: "event-1",
//                     sortId: 1,
//                     type: "map",
//                     config: {
//                         mapData: mapData,
//                         contentItems: [
//                             {
//                                 identifier: "contactData",
//                                 sortId: 1,
//                                 type: "contact-data",
//                                 config: {
//                                     items: contactData
//                                 }
//                             }
//                         ]
//                     }
//                 };
//                 resolve([map]);
//             });
//         }
//         return new Promise<IContentItem<IImgProps>[]>((resolve) => {
//             resolve([{
//                 identifier: "no-content-available",
//                 sortId: 1,
//                 type: "no-content-available",
//                 config: {
//                     image: {
//                         identifier: "no-content-img",
//                         position: "left",
//                         caption: "Photo by Vlad Tchompalov on Unsplash",
//                         src: "/static/images/vlad-tchompalov-dQkXoqQLn40-unsplash.jpg"
//                     },
//                     link: {
//                         identifier: "home",
//                         href: ""
//                     }
//                 }
//             }]);
//         });
//     }
// }