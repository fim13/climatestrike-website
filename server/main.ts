import next from "next";
import express from "express";
import helmet from "helmet";
import bodyParser from "body-parser";
import nextI18NextMiddleware from "next-i18next/middleware";
import dotenv from "dotenv";

import NextI18NextInstance from "../core/I18n";
import {MailchimpService} from "./services/MailchimpService";
import {log} from "./services/Logger";
import {config} from "../config/config";
import {DataService} from "./services/DataService";
import {ICacheOptions} from "./core/@def";
import {PageModelFactory} from "./core/PageModelFactory";


const port = parseInt(process.env.PORT || "3000", 10);
const dev = process.env.NODE_ENV !== "production";
const app = next({dev});
const handle = app.getRequestHandler();

if (dev) {
    dotenv.config();
}

(async () => {
    await app.prepare();
    const server = express();
    server.use(helmet());
    server.use(bodyParser.urlencoded({extended: true}));
    server.use(bodyParser.json());
    const $mc = new MailchimpService(
        process.env.MAILCHIMP_API_KEY,
        process.env.MAILCHIMP_LIST_ID,
        {email: "EMAIL", zip: "POSTLEITZA", language: "MMERGE5", firstname: "FNAME", lastname: "LNAME"}
    );

    server.post("/register-newsletter", (req, res) => {
        $mc.register(req.body)
            .then(() => {
                res.sendStatus(200);
            })
            .catch(() => {
                res.sendStatus(500);
            });
    });

    const cacheOptions: ICacheOptions = {
        enabled: !dev,
        max: 200
    };
    const dataService = new DataService({cache: cacheOptions});
    const dataSourceStrategy = new PageModelFactory().create(config.dataSources.strategy, config.dataSources.config);
    dataService.setDelegate(dataSourceStrategy);

    server.post("/pages", (_req, res) => {
        log.debug("Pages requested");
        dataService.getPages()
            .then((pages) => {
                res.send(pages);
            })
            .catch(err => {
                log.error(`Error requesting pages: ${err}`);
                res.sendStatus(500);
            });
    });

    server.post("/content", (req, res) => {
        const {identifier, languageId} = req.body;
        log.debug(`Content requested for ${identifier} in ${languageId}`);
        dataService.getContent(identifier, languageId)
            .then((content) => {
                res.send(content);
            })
            .catch(err => {
                log.error(`Error requesting content for slug '${identifier}' and language '${languageId}': ${err}`);
                res.sendStatus(500);
            });
    });

    server.use(nextI18NextMiddleware(NextI18NextInstance));
    server.get("*", (req, res) => handle(req, res));
    await server.listen(port);
    log.info(`> Server listening at http://localhost:${port} as ${dev ? "development" : process.env.NODE_ENV}`);
})();