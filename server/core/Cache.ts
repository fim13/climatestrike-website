import LRU from "lru-cache";
import {ICache, ICacheOptions} from "./@def";

export class Cache implements ICache {
    private cache: LRU<string, any>;

    constructor(opts: ICacheOptions) {
        if (opts.enabled) {
            this.cache = new LRU({max: opts.max, maxAge: opts.maxAge});
        }
    }

    public async get<T>(key: string, provider: () => Promise<T>) {
        if (this.cache) {
            let result = this.cache.get(key);
            if (!result) {
                result = await provider();
                this.cache.set(key, result);
            }
            return Promise.resolve(result);
        }
        return provider();
    }

    public clear() {
        this.cache.reset();
    }
}