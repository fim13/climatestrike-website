import {ICacheOptions} from "../core/@def";

export interface ILogger {
    info(msg: any): void;
    debug(msg: any): void;
    warn(msg: any): void;
    error(msg: any): void;
}

export interface IMailchimpService {
    register(data: any): Promise<void>;
}

export interface IDataServiceOptions {
    cache: ICacheOptions;
}