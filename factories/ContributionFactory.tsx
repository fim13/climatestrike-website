import React from "react";
import {Grid} from "@material-ui/core";
import {filter, includes, map, cloneDeep, remove, ceil} from "lodash";
import classNames from "classnames";

import {IPage, ELinkLocation} from "../interfaces/model";
import {IContribution, IAppContext, II18nProps} from "../components/@def";
import {LanguageSelector} from "../components/Selector";
import {SocialLinks} from "../components/SocialLinks";
import {Link, withTranslation} from "../core/I18n";
import {FormDialog} from "../components/FormDialog";


interface ICsAProps {
    page: IPage;
}
const TranslationSpan = withTranslation()<ICsAProps & II18nProps>(({page, t}) => {
    return <span className={page.identifier}>{t(page.identifier)}</span>;
});

const reducePages = (pages: IPage[], location: ELinkLocation) => {
    const filtered = filter(pages, page => includes(page.config.linkLocations, location));
    const mapped = map(filtered, (page) => {
        return {
            id: page.id,
            slug: page.slug,
            identifier: page.identifier,
            menuItem: () => {return <TranslationSpan page={page} />;}
        };
    });
    return mapped;
};

const distributeToColumns = (items: any[], n: number) => {
    const toDistribute = cloneDeep(items);
    let i = n;
    const distributed = [];
    while (i > 0) {
        const nColumn = ceil(toDistribute.length / i);
        const columnItems = remove(toDistribute, (_item, index) => index < nColumn);
        distributed.push(columnItems);
        i--;
    }
    return distributed;
};

export class ContributionFactory {
    public create(strategy: string, context: IAppContext, config: any): IContribution {
        switch (strategy) {
            case "languages":
                return {
                    identifier: strategy, provider: () => <LanguageSelector items={config.languages} />
                };
            case "social":
                return {identifier: strategy, provider: () => <SocialLinks {...config} />};
            case "form-dialog":
                return {identifier: strategy, provider: () => <FormDialog {...config} />};
            case "icon-menu-item":
                return {
                    identifier: strategy, provider: () =>
                        <Link key={strategy} href={config.href}><a><img className="layout-home-icon" src={config.src} /></a></Link>
                };
            case "menu-item":
                return {
                    identifier: strategy, provider: () => {
                        const pages = reducePages(context.pages, config.location);
                        return map(pages, (page, index) => {
                            return <div key={"" + index} className={classNames({selected: page.identifier === context.activePageIdentifier}, page.slug)}>
                                <Link href={"/" + page.slug}>{<a>{page.menuItem()}</a>}</Link>
                            </div>;
                        });
                    }
                };
            case "link-item":
                return {
                    identifier: strategy, provider: () => {
                        const pages = reducePages(context.pages, config.location);
                        const nColumns = config.nColumns;
                        const columnItems = distributeToColumns(pages, nColumns);
                        return <Grid container>
                            {map(columnItems, (items, index) => {
                                return <Grid key={"" + index} item xs={12} md={6}>
                                    {map(items, (page) => {
                                        return <Link key={page.id} href={"/" + page.slug}>{<div>{page.menuItem()}</div>}</Link>;
                                    })}
                                </Grid>;
                            })}
                        </Grid>;
                    }
                };
            default:
                throw new Error(`ContributionFactory - Cannot find strategy '${strategy}'`);
        }
    }
}