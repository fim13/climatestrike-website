import React from "react";
import {
    Facebook, Instagram, Twitter, YouTube,
    Public, Email, WhatsApp, Telegram, Chat,
    Forward, Menu, Close
} from "@material-ui/icons";
import {SvgIconProps} from "@material-ui/core/SvgIcon/SvgIcon";

export class IconFactory {
    private static _instance: IconFactory = new IconFactory();
    public static instance() {
        return IconFactory._instance;
    }

    public create(type: string, props?: SvgIconProps) {
        switch (type) {
            case "facebook": return <Facebook {...props} />;
            case "instagram": return <Instagram {...props} />;
            case "twitter": return <Twitter {...props} />;
            case "youtube": return <YouTube {...props} />;

            case "website": return <Public {...props} />;
            case "email": return <Email {...props} />;
            case "whatsapp": return <WhatsApp {...props} />;
            case "telegram": return <Telegram {...props} />;
            case "discord": return <Chat {...props} />;
            case "other": return <Forward {...props} />;

            case "menu": return <Menu {...props} />;
            case "close": return <Close {...props} />;

            default:
                throw new Error("IconFactory - No icon found: " + type);
        }

    }
}