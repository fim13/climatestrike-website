import {TObjectId, IDictionary} from "./core";

interface IStrategy<T = any> {
    strategy: string;
    config: T;
}

export interface IMenuConfig {
    contributions: IStrategy[];
}

export interface ISubMenuConfig {
    contributions: IStrategy[];
}

export interface IFooterConfig {
    contributions: IStrategy[];
}

interface ILanguageConfig {
    languages: TObjectId[];
    default: TObjectId;
    paths?: IDictionary<string>;
}

interface IColorConfig {
    default: string[];
}

export interface IConfiguration {
    dataSources: IStrategy;
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    languages: ILanguageConfig;
    colors: IColorConfig;
}