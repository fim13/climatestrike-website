// next.config.js 
const withFonts = require("next-fonts");
const withLess = require("@zeit/next-less");

module.exports = withFonts(withLess({
    lessLoaderOptions: {
        javascriptEnabled: true
    },
    webpack(config) {
        return config;
    }
}));