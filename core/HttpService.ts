import {IHttpService} from "../interfaces/services";
import Axios, {AxiosResponse, AxiosRequestConfig} from "axios";


export class HttpService implements IHttpService {
    private static _instance: IHttpService = new HttpService();
    private delegate = Axios;

    public static instance(): IHttpService {
        return HttpService._instance;
    }

    public get<T>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.delegate.get(url, config);
    }

    public post<T>(url: string, data: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.delegate.post(url, data, config);
    }

    public getd<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        return this.dispatch(() => this.delegate.get(url, config));
    }

    public postd<T>(url: string, data: any, config?: AxiosRequestConfig): Promise<T> {
        return this.dispatch(() => this.delegate.post(url, data, config));
    }

    private async dispatch<T>(dispatcher: () => Promise<AxiosResponse<T>>): Promise<T> {
        try {
            const response = await dispatcher();
            if (response.status < 400) {
                return Promise.resolve(response.data);
            }
            return Promise.reject("HttpService - Error fetching data: " + response.request.url);
        }
        catch (err) {
            return Promise.reject("HttpService - Error fetching data: " + err);
        }
    }
}