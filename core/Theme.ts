import {createMuiTheme} from "@material-ui/core/styles";

let _theme = createMuiTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 768,
            md: 960,
            lg: 1280,
            xl: 1920
        }
    },
    palette: {
        primary: {main: "#FFF"},
        secondary: {main: "#E57474"}
    }
});

export const theme = _theme;