# Climatestrike Website
## General information
This project contains the code for the new climatestrike.ch website.
The goal is to implement the design proposed [here](https://www.figma.com/file/25KpJ6rzDG4KpVguoPm0tl/REDESIGN-3.0?node-id=0%3A1), the final design elements you can find [here](https://www.figma.com/file/70cISswQ02B9MciG9s5vq6/Design-Elements-Final?node-id=0%3A1). The new design is developed on branch `feature/redesign`.

There was a previous design, which you can find [here](https://www.figma.com/proto/gS764DdLo3IkDJgHfni6fd/Climatestrike-Website-new-Prototype?node-id=260%3A0&viewport=299%2C362%2C0.019355816766619682&scaling=scale-down). The `master` branch represents the last state before starting the redesign.

## Getting started
```
git clone https://gitlab.com/omg_me/climatestrike-website.git
# Assuming you want to work on the website before the redesign:
git checkout --track origin/feature/redesign
# Just to be sure
git pull
cd climatestrike-website
npm i
```

Define the environment variables
```
cp .env_dist .env
```

### Development
For development, run:
```
npm run dev
```
in the root folder. Then open
```
localhost:3000
```
in your browser.

#### Configuration
By default we use `strapi` as a headless CMS. Theoretically we could add support for all kinds of CMS, but that might be done a lot later.

### Production
For a production install, use
```
npm run build
# Temporary solution to keep the process running in the background
NODE_ENV=production nohup npm run dev > /dev/null &
```

## I want to help out with...
### Styling
Once you got your project running, you are most welcome to style components. The goal is that the page will look as designed [here](https://www.figma.com/file/25KpJ6rzDG4KpVguoPm0tl/REDESIGN-3.0?node-id=0%3A1) and [here](https://www.figma.com/file/70cISswQ02B9MciG9s5vq6/Design-Elements-Final?node-id=0%3A1).

We use [`.less`](http://lesscss.org/) instead of `.css` files, but if you don't like or know `less` you can simply add `css`, because `less` will handle your `.css` styles.

The styles are located in `./styles`, usually one file per component. If you add a new file, you need to register it in `./styles/styles.less` (just follow the code already in there) for it to get loaded into the browser.

### Implementing components
More to follow...

### Working on the backend
More to follow...

### Deployment
More to follow...

## Troubleshooting
On Linux, if you get an error like `Watchpack Error (watcher): Error: ENOSPC: System limit for number of file watchers reached, watch <some-dir>`, you can increase your file watchers limit using

```
sudo sysctl fs.inotify.max_user_watches=<some-number>
sudo sysctl -p
```
or to make it permanent
```
echo fs.inotify.max_user_watches=<some-number> | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```

You can print the current limit with `cat /proc/sys/fs/inotify/max_user_watches`.

## Contributions
See [CONTRIBUTING](CONTRIBUTING.md).

## License
See [LICENSE](LICENSE).