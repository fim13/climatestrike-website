import * as React from "react";
import {map} from "lodash";

import {IUrlObject} from "../interfaces/core";
import {IconFactory} from "../factories/IconFactory";


interface ISocialLinksProps {
    social: IUrlObject[];
}

export const SocialLinks: React.FC<ISocialLinksProps> = ({social}) => {
    const factory = IconFactory.instance();
    return <div className="social-links">
        {map(social, (socialItem) => <a key={socialItem.id} href={socialItem.url} target="_blank" rel="noopener noreferrer">
            {factory.create(socialItem.id)}
        </a>)}
    </div>;
};

