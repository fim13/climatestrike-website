import React from "react";
import classNames from "classnames";
import uuid from "uuid";
import {isEmpty, intersection, filter, map, first, sortBy, startsWith} from "lodash";

import {IContactDataProps, IPageContextProps, II18nProps} from "./@def";
import {withTranslation} from "../core/I18n";
import {IconFactory} from "../factories/IconFactory";


const _ContactData: React.FC<IContactDataProps & II18nProps & IPageContextProps> = (props) => {
    const {context, items, selection} = props;
    const className = classNames("contact-data", context.itemId);
    const hasSelection = !isEmpty(selection);
    let data = filter(items, item => hasSelection ? !isEmpty(intersection(item.identifiers, selection)) : item.default);
    const iconFactory = IconFactory.instance();
    return <div className={className} >
        {map(data, (dp) => {
            const content = sortBy(dp.content, (content) => content.type);
            return <div key={uuid.v4()} className={classNames("content-group", ...dp.identifiers)}>
                <h1 key="contact-header">{dp.description || props.t(first(selection)) || props.t(first(dp.identifiers))}</h1>
                <div key={uuid.v4()} className="contact-link-group">
                    {map(content, item => {
                        const href = item.type === "email" && !startsWith(item.link, "mailto:") ? "mailto:" + item.link : item.link;
                        return <div key={uuid.v4()} className={classNames("contact-link-item", {"with-name": !!item.linkName})}>
                            <a href={href} target="_blank" rel="noopener noreferrer">
                                {iconFactory.create(item.type)}
                                <div key="link-name" className="link-name">{item.linkName}</div>
                            </a>
                        </div>;
                    })}
                </div>
            </div>;
        })}
    </div>;
};

export const ContactData = withTranslation()<IContactDataProps & II18nProps>(_ContactData);