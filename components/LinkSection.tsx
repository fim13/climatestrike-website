import React from "react";
import classNames from "classnames";
import {map} from "lodash";

import {ILinkSectionProps, IPageContextProps, II18nProps, EButtonStyle} from "./@def";
import {renderTitle, renderDescription, renderLink} from "./BaseComponents";
import {getLanguageText, withTranslation} from "../core/I18n";


export const _LinkSection: React.FC<ILinkSectionProps & IPageContextProps & II18nProps> = (props) => {
    const {context, title, description, link, i18n} = props;
    const className = classNames("link-section", context.itemId);
    const renderers = [
        () => renderTitle(getLanguageText(title, i18n.language)),
        () => renderDescription(getLanguageText(description, i18n.language)),
        () => renderLink({style: EButtonStyle.primary, href: link["link_" + i18n.language]})
    ];
    return <div className={className} >
        {map(renderers, renderer => renderer())}
    </div>;
};

export const LinkSection = withTranslation()(_LinkSection);