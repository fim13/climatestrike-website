import React from "react";
import {map, forEach, cloneDeep, merge, find} from "lodash";
import {Grid} from "@material-ui/core";
import classNames from "classnames";

import {IPageContext, IAbstractPageProps} from "./@def";
import {IContentItem} from "../interfaces/model";
import {ContentFactory} from "../factories/ContentFactory";
import {ColorService} from "../core/ColorService";


const getRowModel = (items: IContentItem[], config: number[]) => {
    const rowModel: IContentItem[][] = [];
    let currentRow = [];
    let configIndex = 0;
    forEach(items, (item, index) => {
        currentRow.push(item);
        if (currentRow.length == config[configIndex]) {
            rowModel.push(cloneDeep(currentRow));
            currentRow = [];
            configIndex = (configIndex + 1) % config.length;
        }
        if (index === items.length - 1 && currentRow.length !== 0) {
            rowModel.push(currentRow);
        }
    });
    return rowModel;
};

export const AbstractPage: React.FC<IAbstractPageProps> = (props) => {
    const factory = new ContentFactory();
    const $color = new ColorService();
    const {activePageIdentifier, items, pages} = props;
    const page = find(pages, (_page) => _page.identifier == activePageIdentifier);
    const context: IPageContext = {itemId: null, identifier: activePageIdentifier, items: items, rowItems: []};
    const rowModel = getRowModel(items, page.config.rowModel ? page.config.rowModel : [1]);
    const spacing = 3;
    return <div key={activePageIdentifier} className={classNames("page", activePageIdentifier)} >
        {map(rowModel, (row, index) => <Grid key={index} container spacing={spacing}>
            {map(row, item => <Grid key={"" + index + "-" + item.identifier} item xs={12} md={(12 / row.length) as any}>
                {factory.create(
                    item.type,
                    merge({image: {color: $color.getNextDefault()}}, item.config),
                    merge({}, context, {rowItems: row, itemId: item.identifier})
                )}
            </Grid>)}
        </Grid>)}
    </div >;
};