import React from "react";
import Head from "next/head";
import {map} from "lodash";
import {ThemeProvider} from "@material-ui/core";

import {Header} from "./Header";
import {Footer} from "./Footer";
import {ContributionFactory} from "../factories/ContributionFactory";
import {IPageWrapperProps, II18nProps, } from "./@def";
import {withTranslation} from "../core/I18n";
import {theme} from "../core/Theme";

import "../styles/styles.less";



const Layout: React.FC<IPageWrapperProps & II18nProps> = (props) => {
    const {title = "Climatestrike", activePageIdentifier, isMobile, pages, menu, submenu, footer, children} = props;
    const appContext = {activePageIdentifier, pages};
    const contributionFactory = new ContributionFactory();
    const menuContributions = map(menu.contributions, (contribution) => contributionFactory.create(contribution.strategy, appContext, contribution.config));
    const submenuContributions = map(submenu.contributions, (contribution) => contributionFactory.create(contribution.strategy, appContext, contribution.config));
    const footerContributions = map(footer.contributions, (contribution) => contributionFactory.create(contribution.strategy, appContext, contribution.config));
    const layoutProps = {
        activePageIdentifier: activePageIdentifier,
        isMobile: isMobile,
        menuContributions: menuContributions,
        submenuContributions: submenuContributions,
        footerContributions: footerContributions,
    };
    return (
        <div className="root">
            <ThemeProvider theme={theme}>
                <Head>
                    <title>{props.t(title)}</title>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <link rel="shortcut icon" href="/static/images/favicon.ico" />
                </Head>
                <Header {...layoutProps} />
                {children}
                <Footer contributions={footerContributions} />
            </ThemeProvider>
        </div>
    );
};

export default withTranslation()<IPageWrapperProps & II18nProps>(Layout);
