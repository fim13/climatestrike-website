import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";

import {withTranslation} from "../core/I18n";
import {IFormProps, II18nProps, EButtonStyle} from "./@def";
import Button from "./Button";


const _FormDialog: React.FC<IFormProps & II18nProps> = (props) => {
    const {t, submitText} = props;
    const title = t(submitText);
    const [open, setOpen] = useState(false);
    return <React.Fragment>
        <Button style={EButtonStyle.primary} type={submitText} onClick={() => setOpen(true)} />
        <Dialog open={open}>
            <MuiDialogTitle classes={{root: "dialog-title"}}>
                {title}
            </MuiDialogTitle>
            <MuiDialogContent classes={{root: "dialog-content"}} dividers>
            </MuiDialogContent>
        </Dialog>
    </React.Fragment>;
};

export const FormDialog = withTranslation()<IFormProps & II18nProps>(_FormDialog);