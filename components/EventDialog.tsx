import React from "react";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import {map} from "lodash";
import moment from "moment";

import {withTranslation} from "../core/I18n";
import {IEventDialogProps, II18nProps, IEventGroup} from "./@def";
import {IconFactory} from "../factories/IconFactory";

const TIME_FORMAT = "DD.MM.YYYY, HH:mm";
const _EventDialog: React.FC<IEventDialogProps & {dialogData: IEventGroup} & II18nProps> = (props) => {
    const iconFactory = IconFactory.instance();
    const {dialogData, setDialogData, t} = props;
    if (!dialogData) return null;
    const model = [
        {id: "event-start", condition: (data: IEventGroup) => !!data.startTime, renderer: (data: IEventGroup) => <span>{`${moment(data.startTime).format(TIME_FORMAT)}`}</span>},
        {id: "event-end", condition: (data: IEventGroup) => !!data.endTime, renderer: (data: IEventGroup) => <span>{`${moment(data.endTime).format(TIME_FORMAT)}`}</span>},
        {id: "event-description", condition: (data: IEventGroup) => !!data.description, renderer: (data: IEventGroup) => <span>{data.description}</span>},
        {id: "event-location", condition: (data: IEventGroup) => data.events.length === 1, renderer: (data: IEventGroup) => <span>{map(data.events, event => event.location)}</span>},
        {id: "event-locations", condition: (data: IEventGroup) => data.events.length > 1, renderer: (data: IEventGroup) => <span>{map(data.events, event => event.location)}</span>},
    ];
    return <Dialog open onClose={() => setDialogData(null)}>
        <MuiDialogTitle classes={{root: "dialog-title"}}>
            {dialogData.title}
            {iconFactory.create("close", {onClick: () => setDialogData(null)})}
        </MuiDialogTitle>
        <MuiDialogContent classes={{root: "dialog-content"}} dividers>
            {map(model, (def) => {
                if (!def.condition(dialogData)) return null;
                return <div key={def.id}>
                    <h1>{t(def.id)}</h1>
                    {def.renderer(dialogData)}
                </div>;
            })}
        </MuiDialogContent>
    </Dialog >;
};

export const EventDialog = withTranslation()(_EventDialog);