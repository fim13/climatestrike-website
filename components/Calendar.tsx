import React from "react";
import classNames from "classnames";
import {Calendar as BigCalendar, momentLocalizer} from "react-big-calendar";
import moment from "moment";

import {IEventsProps, IPageContextProps, II18nProps, IEventDialogProps, IEventGroup} from "./@def";
import {withTranslation} from "../core/I18n";


const _Calendar: React.FC<IEventsProps & IPageContextProps & IEventDialogProps & II18nProps> = (props) => {
    const language = props.i18n.language;
    const {context, items, setDialogData} = props;
    moment.locale("ch-" + language, {
        week: {dow: 1, doy: 4}
    });
    const localizer = momentLocalizer(moment);
    const className = classNames(context.itemId);
    return <BigCalendar
        className={className}
        localizer={localizer}
        events={items}
        views={["month", "week"]}
        defaultView="week"
        onSelectEvent={(e: IEventGroup) => setDialogData(e)}
        startAccessor={(e: IEventGroup) => new Date(e.startTime)}
        endAccessor={(e: IEventGroup) => new Date(e.endTime)}
    />;
};

export const Calendar = withTranslation()(_Calendar);