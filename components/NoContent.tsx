import React from "react";

import {IImgProps, IPageContextProps, II18nProps} from "./@def";
import {renderTitle, renderDescription} from "./BaseComponents";
import {withTranslation, getLanguageText} from "../core/I18n";


const _NoContent: React.FC<IImgProps & IPageContextProps & II18nProps> = (props) => {
    const {image, t, i18n} = props;
    return <div className="no-content">
        <div className="image" style={{backgroundImage: `linear-gradient(rgba(255, 255, 255, 1), rgba(255, 255, 255, 0) 40%), url('${image.src}')`}}>
            {renderTitle(t("no-content"))}
        </div>
        {image.caption ? renderDescription(getLanguageText(image.caption, i18n.language)) : null}
    </div>;
};

export const NoContent = withTranslation()(_NoContent);