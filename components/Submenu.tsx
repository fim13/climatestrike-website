import * as React from "react";
import {map, isFunction} from "lodash";
import classNames from "classnames";

import {ISubMenuProps} from "./@def";


export const Submenu: React.FC<ISubMenuProps> = ({contributions, className}) => {
    return <div className={classNames("submenu", className)}>
        {map(contributions, (contribution) => {
            const className = classNames("contribution-container", contribution.identifier);
            return <div key={contribution.identifier} className={className}>
                {isFunction(contribution.provider) ? contribution.provider() : null}
            </div>;
        })}
    </div>;
};