import * as React from "react";
import {NextPage, NextPageContext} from "next";
import absoluteUrl from "next-absolute-url";
import {find} from "lodash";

import Layout from "./Layout";
import {config} from "../config/config";
import {IPageWrapperProps} from "./@def";
import NextI18NextInstance from "../core/I18n";
import {HttpService} from "../core/HttpService";
import {IPage, IContentItem} from "../interfaces/model";
// import {get, find} from "lodash";
// import {isString} from "util";


const PageWrapper = (Page: NextPage<any>) => {
    return class Pg extends React.Component<IPageWrapperProps> {
        static async getInitialProps(args: NextPageContext) {
            const $http = new HttpService();
            const {origin} = absoluteUrl(args.req);
            const pages = await $http.postd<IPage[]>(`${origin}/pages`, {});
            const currentLanguage = args.req ? (args.req as any).language : NextI18NextInstance.i18n.language;
            const isMobileView = !!(args.req
                ? args.req.headers["user-agent"]
                : navigator.userAgent).match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i);
            let slug = args.pathname.replace("/", "");
            const activePage = find(pages, (page) => page.slug === slug);
            // Super hacky workaround for for dynamic catch-all route.
            // See https://github.com/zeit/next.js/issues/9081 for more info / check if resolved
            // The page flickers and the request returns a 404. Some more investigation is needed.
            // For it to work rename __error.tsx to _error.tsx
            // const route = get(args.req, "params.0");
            // else if (route && isString(route)) {
            //     const slug = route.replace("/", "");
            //     if (pageId === "_error" && find(pages, (page) => page.slug === slug)) {
            //         console.log("Redirecting from _error to " + slug);
            //         pageId = slug;
            //     }
            // }
            const content = await $http.postd<IContentItem[]>(`${origin}/content`, {identifier: activePage.identifier, languageId: currentLanguage});
            const wrapperProps: IPageWrapperProps = {
                title: activePage.identifier,
                isMobile: isMobileView,
                activePageIdentifier: activePage.identifier,
                pages: pages,
                menu: config.menu,
                submenu: config.submenu,
                footer: config.footer,
                items: content
            };
            return {
                ...wrapperProps,
                ...(Page.getInitialProps ? await Page.getInitialProps(args) : null)
            };
        }

        public render() {
            return (
                <Layout {...this.props}>
                    <Page {...this.props} />
                </Layout>
            );
        }
    };
};

export default PageWrapper;
