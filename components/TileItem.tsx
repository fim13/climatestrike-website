import React from "react";
import classNames from "classnames";
import {Grid} from "@material-ui/core";

import {ITileItemProps, IPageContextProps, II18nProps} from "./@def";
import {renderImage, renderDescription, renderTitle, renderSubtitle} from "./BaseComponents";
import {DetectionService} from "../core/DetectionService";
import {withTranslation, getLanguageText} from "../core/I18n";


const _TileItem: React.FC<ITileItemProps & IPageContextProps & II18nProps> = (props) => {
    const {context, title, subtitle, description, icon, image, i18n} = props;
    const languageId = i18n.language;
    const className = classNames("tile-item", context.itemId);
    return <Grid className={className} container spacing={3} >
        <Grid className="tile-container" item xs={12} sm={8}>
            <Grid container spacing={3}>
                <Grid className="tile-icon" item xs={12} sm={4}>{renderImage(icon)}</Grid>
                <Grid className="tile-headers" item xs={12} sm={8}>
                    {[renderTitle(getLanguageText(title, languageId)), renderSubtitle(getLanguageText(subtitle, languageId))]}
                </Grid>
            </Grid>
            <Grid className="tile-description" container>
                {renderDescription(getLanguageText(description, languageId))}
            </Grid>
        </Grid>
        {!DetectionService.instance().isSmall() && <Grid className="tile-image" item sm={4}>{renderImage(image)}</Grid>}
    </Grid>;
};

export const TileItem = withTranslation()(_TileItem);